package com.dindinn.merchant.ui.main.fragments.search

import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import com.dindinn.merchant.R
import com.dindinn.merchant.TestComponentRule
import com.dindinn.merchant.util.SearchViewTyper.typeSearchViewText
import com.dindinn.merchant.util.TabSelector.matchCurrentTabTitle
import com.dindinn.merchant.util.TabSelector.matchTabTitleAtPosition
import com.dindinn.merchant.util.TabSelector.selectTabAtPosition
import com.dindinn.merchant.util.VPMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain


class SearchFragmentTest {
    private val component =
        TestComponentRule(InstrumentationRegistry.getInstrumentation().targetContext)

    @get:Rule
    val chain: RuleChain = RuleChain.outerRule(component)

    @Before
    fun setup() {

    }

    @Test
    fun categoriesAvailable_shouldDisplay() {
        launchFragmentInContainer<SearchFragment>(Bundle(), R.style.Theme_Merchant)
        onView(withId(R.id.fragment_search_tabs))
            .check(matches(isDisplayed()))
            .check(matches(matchTabTitleAtPosition("MAIN", 0)))
            .check(matches(matchTabTitleAtPosition("SIDES", 1)))
            .check(matches(matchTabTitleAtPosition("DRINKS", 2)))
            .check(matches(matchCurrentTabTitle("MAIN")))
            .perform(selectTabAtPosition(1))
            .check(matches(matchCurrentTabTitle("SIDES")))
            .perform(selectTabAtPosition(2))
            .check(matches(matchCurrentTabTitle("DRINKS")))
            .perform(selectTabAtPosition(0))
            .check(matches(matchCurrentTabTitle("MAIN")))
            .perform(selectTabAtPosition(1))
            .check(matches(matchCurrentTabTitle("SIDES")))


        onView(withId(R.id.fragment_search_sv))
            .check(matches(isDisplayed()))
            .perform(
                typeSearchViewText("f")
            )
        onView(withId(R.id.fragment_search_tabs))
            .perform(selectTabAtPosition(1))

        SystemClock.sleep(500)

        onView(withId(R.id.fragment_search_pager))
            .check(
                matches(
                    VPMatcher.matchViewPager2OnView(
                        hasChildCount(2),
                        R.id.fragment_ingredient_rv
                    )
                )
            )

        onView(withId(R.id.fragment_search_tabs))
            .perform(selectTabAtPosition(0))

        SystemClock.sleep(500)

        onView(withId(R.id.fragment_search_pager))
            .check(
                matches(
                    VPMatcher.matchViewPager2OnView(
                        hasChildCount(1),
                        R.id.fragment_ingredient_rv
                    )
                )
            )

        onView(withId(R.id.fragment_search_tabs))
            .perform(selectTabAtPosition(2))

        SystemClock.sleep(500)

        onView(withId(R.id.fragment_search_pager))
            .check(
                matches(
                    VPMatcher.matchViewPager2OnView(
                        hasChildCount(2),
                        R.id.fragment_ingredient_rv
                    )
                )
            )



    }
}
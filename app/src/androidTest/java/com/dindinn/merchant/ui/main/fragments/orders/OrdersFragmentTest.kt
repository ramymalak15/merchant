package com.dindinn.merchant.ui.main.fragments.orders

import android.os.Bundle
import android.os.SystemClock
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import com.dindinn.merchant.R
import com.dindinn.merchant.TestComponentRule
import com.dindinn.merchant.util.RVClicker.clickOnViewChild
import com.dindinn.merchant.util.RVMatcher.atPositionOnView
import com.dindinn.merchant.util.ToastMatcher
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain


class OrdersFragmentTest {
    private val component =
        TestComponentRule(InstrumentationRegistry.getInstrumentation().targetContext)

    @get:Rule
    val chain: RuleChain = RuleChain.outerRule(component)

    @Before
    fun setup() {

    }

    @Test
    fun ordersAvailable_shouldDisplay() {
        //First Scenario: Load Data test
        launchFragmentInContainer<OrdersFragment>(Bundle(), R.style.Theme_Merchant)
        onView(withId(R.id.fragment_orders_rv))
            .check(matches(isDisplayed()))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        withText("#10"),
                        R.id.item_order_order_number
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        0,
                        withText("8 Items"),
                        R.id.item_order_total_tv
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        0,
                        withText(R.string.item_order_accept),
                        R.id.item_order_accept_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
            .check(
                matches(
                    atPositionOnView(
                        1,
                        withText("#11"),
                        R.id.item_order_order_number
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        1,
                        withText("1 Item"),
                        R.id.item_order_total_tv
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        1,
                        withText(R.string.item_order_accept),
                        R.id.item_order_accept_btn
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        1,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            ).check(
                matches(
                    atPositionOnView(
                        1,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )

        SystemClock.sleep(5000)

        onView(withText(R.string.order_reminder))
            .inRoot(ToastMatcher().apply {
                matches(isDisplayed())
            })

        onView(withId(R.id.fragment_orders_rv))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
            .check(
                matches(
                    atPositionOnView(
                        1,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
        SystemClock.sleep(2000)
        onView(withId(R.id.fragment_orders_rv))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
            .check(
                matches(
                    atPositionOnView(
                        1,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (isDisplayed()),
                        R.id.item_order_expired_btn
                    )
                )
            )
        SystemClock.sleep(3000)

        onView(withText(R.string.order_reminder))
            .inRoot(ToastMatcher().apply {
                matches(isDisplayed())
            })

        SystemClock.sleep(2000)
        onView(withId(R.id.fragment_orders_rv))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
            .check(
                matches(
                    atPositionOnView(
                        1,
                        ((isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (isDisplayed()),
                        R.id.item_order_expired_btn
                    )
                )
            )
    }

    @Test
    fun clickBeforeAndAfterExpire_shouldStopOrRemove() {
        //Second Scenario : Accept/Expire the Order
        launchFragmentInContainer<OrdersFragment>(Bundle(), R.style.Theme_Merchant)
        onView(withId(R.id.fragment_orders_rv))
            .check(matches(isDisplayed()))
            .check(matches(hasChildCount(2)))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        withText(R.string.item_order_accept),
                        R.id.item_order_accept_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (isDisplayed()),
                        R.id.item_order_accept_btn
                    )
                )
            )
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>
                        (0, clickOnViewChild(R.id.item_order_accept_btn))
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        withText(R.string.order_accepted),
                        R.id.item_order_accept_btn
                    )
                )
            )

        SystemClock.sleep(5000)


        onView(withText(R.string.order_reminder))
            .inRoot(ToastMatcher().apply {
                matches(not(isDisplayed()))
            })

        SystemClock.sleep(2000)

        onView(withId(R.id.fragment_orders_rv))
            .check(matches(isDisplayed()))
            .check(
                matches(
                    atPositionOnView(
                        0,
                        withText(R.string.order_accepted),
                        R.id.item_order_accept_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (not(isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        0,
                        (isDisplayed()),
                        R.id.item_order_accept_btn
                    )
                )
            )
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
        SystemClock.sleep(3000)

        onView(withText(R.string.order_reminder))
            .inRoot(ToastMatcher().apply {
                matches(isDisplayed())
            })

        SystemClock.sleep(5000)

        onView(withId(R.id.fragment_orders_rv))
            .check(matches(isDisplayed()))
            .check(
                matches(
                    atPositionOnView(
                        1,
                        ((isDisplayed())),
                        R.id.item_order_expired_btn
                    )
                )
            )
            .check(
                matches(
                    atPositionOnView(
                        1,
                        (not(isDisplayed())),
                        R.id.item_order_accept_btn
                    )
                )
            ).perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>
                        (1, clickOnViewChild(R.id.item_order_expired_btn))
            )
            .check(matches(hasChildCount(1)))


    }
}
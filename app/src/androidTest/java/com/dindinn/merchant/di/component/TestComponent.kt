package com.dindinn.merchant.di.component

import android.app.Application
import android.content.Context
import com.dindinn.merchant.MerchantApplication
import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.di.ApplicationContext
import com.dindinn.merchant.di.module.ApplicationTestModule
import com.dindinn.merchant.utils.rx.SchedulerProvider
import dagger.Component
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationTestModule::class])
interface TestComponent : ApplicationComponent {
    override fun inject(app: MerchantApplication)

    override fun getApplication(): Application

    @ApplicationContext
    override fun getContext(): Context

    override fun getSchedulerProvider(): SchedulerProvider

    override fun getCompositeDisposable(): CompositeDisposable

    override fun getNetworkService(): NetworkService
}
package com.dindinn.merchant.util

import android.view.View
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.viewpager2.widget.ViewPager2
import org.hamcrest.Description
import org.hamcrest.Matcher


object VPMatcher {

    fun matchViewPager2OnView(
        itemMatcher: Matcher<View>,
        targetViewId: Int
    ): Matcher<View> {

        return object : BoundedMatcher<View, ViewPager2>(ViewPager2::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("get view id $itemMatcher")
            }

            public override fun matchesSafely(viewPager2: ViewPager2): Boolean {
                val currentView = viewPager2.getChildAt(0)
                val targetView = currentView?.findViewById<View>(targetViewId)
                return itemMatcher.matches(targetView)
            }

        }
    }

}
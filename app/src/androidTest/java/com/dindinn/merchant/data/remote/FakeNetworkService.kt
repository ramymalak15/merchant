package com.dindinn.merchant.data.remote


import com.dindinn.merchant.data.remote.response.*
import io.reactivex.rxjava3.core.Single
import java.util.*

class FakeNetworkService : NetworkService() {
    private val startDate = Date()
    override fun doFetchOrdersList(): Single<OrdersResponse> = Single.just(
        OrdersResponse(
            listOf(
                Data(
                    10,
                    Date(startDate.time),
                    Date(startDate.time + (5000)),
                    Date(startDate.time + (7000)),
                    listOf(
                        MainItem(
                            20,
                            "Chicken Noodle",
                            2,
                            listOf(
                                Addon(26, "Extra chicken", 2),
                                Addon(27, "Sambal", 1)
                            )
                        ),
                        MainItem(
                            21,
                            "Beef Noodle",
                            3,
                            listOf(
                                Addon(26, "Extra Beef", 3),
                                Addon(27, "Sambal", 5)
                            )
                        ),
                        MainItem(
                            22,
                            "Vegetables Noodle",
                            3,
                            listOf(
                                Addon(26, "Extra Vegetables", 4),
                                Addon(27, "Sambal", 6)
                            )
                        )
                    )


                ),
                Data(
                    11,
                    Date(startDate.time),
                    Date(startDate.time + (10000).toInt()),
                    Date(startDate.time + (12000)),
                    listOf(
                        MainItem(
                            20,
                            "Special extra large fried rice",
                            1,
                            listOf(
                                Addon(31, "Fried Egg", 3)
                            )
                        )
                    )
                )
            ),
            Status("success", 200, true)
        )
    )


}
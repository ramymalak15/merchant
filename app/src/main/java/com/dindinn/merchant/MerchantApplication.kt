package com.dindinn.merchant

import android.app.Application
import com.dindinn.merchant.di.component.ApplicationComponent
import com.dindinn.merchant.di.component.DaggerApplicationComponent
import com.dindinn.merchant.di.module.ApplicationModule


class MerchantApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent


    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }


}
package com.dindinn.merchant.di.module

import android.widget.LinearLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dindinn.merchant.di.ViewModelScope
import com.dindinn.merchant.ui.base.BaseItemViewHolder
import com.dindinn.merchant.ui.main.fragments.orders.FragmentOrderSharedViewModel
import com.dindinn.merchant.ui.main.fragments.orders.item_item.ItemAdapter
import com.dindinn.merchant.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable

@Module
class ViewHolderModule(private val viewHolder: BaseItemViewHolder<*, *>) {

    @Provides
    @ViewModelScope
    fun provideLifecycleRegistry(): LifecycleRegistry = LifecycleRegistry(viewHolder)

    @Provides
    fun provideLinearLayoutManager() = LinearLayoutManager(viewHolder.parent.context)


    @Provides
    fun provideItemAdapter() = ItemAdapter(viewHolder.lifecycle, ArrayList())

    @Provides
    fun provideDividerItemDecoration() = DividerItemDecoration(viewHolder.parent.context, LinearLayout.VERTICAL)

    @Provides
    fun provideFragmentOrderSharedViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable
    ): FragmentOrderSharedViewModel = ViewModelProvider(
        (viewHolder.parent.context as FragmentActivity) , ViewModelProviderFactory(FragmentOrderSharedViewModel::class) {
            FragmentOrderSharedViewModel(schedulerProvider, compositeDisposable)
        }).get(FragmentOrderSharedViewModel::class.java)
}
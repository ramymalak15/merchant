package com.dindinn.merchant.di.component

import com.dindinn.merchant.di.ViewModelScope
import com.dindinn.merchant.di.module.ViewHolderModule
import com.dindinn.merchant.ui.main.fragments.orders.item_item.ItemItemViewHolder
import com.dindinn.merchant.ui.main.fragments.orders.order_item.OrderItemViewHolder
import com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item.IngredientItemViewHolder
import dagger.Component

@ViewModelScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewHolderModule::class]
)
interface ViewHolderComponent {
    fun inject(orderItemViewHolder: OrderItemViewHolder)
    fun inject(itemItemViewHolder: ItemItemViewHolder)
    fun inject(ingredientItemViewHolder: IngredientItemViewHolder)
}
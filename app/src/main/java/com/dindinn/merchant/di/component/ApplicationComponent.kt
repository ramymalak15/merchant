package com.dindinn.merchant.di.component

import android.app.Application
import android.content.Context
import com.dindinn.merchant.MerchantApplication
import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.di.ApplicationContext
import com.dindinn.merchant.di.module.ApplicationModule
import com.dindinn.merchant.utils.rx.SchedulerProvider


import dagger.Component
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(app: MerchantApplication)

    fun getApplication(): Application

    @ApplicationContext
    fun getContext(): Context


    fun getSchedulerProvider(): SchedulerProvider

    fun getCompositeDisposable(): CompositeDisposable

    fun getNetworkService():NetworkService
}
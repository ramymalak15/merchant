package com.dindinn.merchant.di.component


import com.dindinn.merchant.di.FragmentScope
import com.dindinn.merchant.di.module.FragmentModule
import com.dindinn.merchant.ui.main.fragments.search.ingredient.IngredientFragment
import com.dindinn.merchant.ui.main.fragments.orders.OrdersFragment
import com.dindinn.merchant.ui.main.fragments.search.SearchFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {
    fun inject(ordersFragment: OrdersFragment)
    fun inject(ordersFragment: IngredientFragment)
    fun inject(searchFragment: SearchFragment)
}
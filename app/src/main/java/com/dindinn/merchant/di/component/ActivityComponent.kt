package com.dindinn.merchant.di.component

import com.dindinn.merchant.di.ActivityScope
import com.dindinn.merchant.di.module.ActivityModule
import com.dindinn.merchant.ui.main.MainActivity


import dagger.Component

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}
package com.dindinn.merchant.di.module

import android.app.Application
import android.content.Context
import com.dindinn.merchant.MerchantApplication
import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.di.ApplicationContext
import com.dindinn.merchant.utils.rx.RxSchedulerProvider
import com.dindinn.merchant.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton


@Module
class ApplicationModule(private val application: MerchantApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(): Context = application

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = RxSchedulerProvider()

    @Provides
    fun provideNetworkService():NetworkService = NetworkService()

}
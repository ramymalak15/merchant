package com.dindinn.merchant.di.module


import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.dindinn.merchant.data.repository.IngredientRepository
import com.dindinn.merchant.data.repository.OrdersRepository
import com.dindinn.merchant.ui.base.BaseFragment
import com.dindinn.merchant.ui.main.fragments.search.ingredient.IngredientViewModel
import com.dindinn.merchant.ui.main.fragments.orders.FragmentOrderSharedViewModel
import com.dindinn.merchant.ui.main.fragments.orders.OrdersViewModel
import com.dindinn.merchant.ui.main.fragments.orders.order_item.OrderAdapter
import com.dindinn.merchant.ui.main.fragments.search.CategoryAdapter
import com.dindinn.merchant.ui.main.fragments.search.SearchIngredientSharedViewModel
import com.dindinn.merchant.ui.main.fragments.search.SearchViewModel
import com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item.IngredientAdapter
import com.dindinn.merchant.utils.rx.SchedulerProvider
import com.google.android.flexbox.*
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable

@Module
class FragmentModule(private val fragment: BaseFragment<*, *>) {

    @Provides
    fun provideOrdersViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        ordersRepository: OrdersRepository
    ): OrdersViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(OrdersViewModel::class) {
            OrdersViewModel(schedulerProvider, compositeDisposable, ordersRepository)
        }).get(OrdersViewModel::class.java)

    @Provides
    fun provideIngredientViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        ingredientRepository: IngredientRepository
    ): IngredientViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(IngredientViewModel::class) {
            IngredientViewModel(
                schedulerProvider,
                compositeDisposable,
                ingredientRepository
            )
        }).get(IngredientViewModel::class.java)

    @Provides
    fun provideSearchIngredientSharedViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
    ): SearchIngredientSharedViewModel = ViewModelProvider(
        fragment.activity!!, ViewModelProviderFactory(SearchIngredientSharedViewModel::class) {
            SearchIngredientSharedViewModel(schedulerProvider, compositeDisposable)
        }).get(SearchIngredientSharedViewModel::class.java)


    @Provides
    fun provideSearchViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        ingredientRepository: IngredientRepository
    ): SearchViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(SearchViewModel::class) {
            SearchViewModel(
                schedulerProvider,
                compositeDisposable,
                ingredientRepository
            )
        }).get(SearchViewModel::class.java)

    @Provides
    fun provideCategoryAdapter() = CategoryAdapter(fragment, ArrayList())

    @Provides
    fun provideIngredientAdapter() = IngredientAdapter(fragment.lifecycle, ArrayList())

    @Provides
    fun provideFlexboxLayoutManager() = FlexboxLayoutManager(fragment.context).apply {
        flexWrap = FlexWrap.WRAP
        flexDirection = FlexDirection.ROW
        alignItems = AlignItems.STRETCH
        justifyContent = JustifyContent.SPACE_AROUND
    }

    @Provides
    fun provideFragmentOrderSharedViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable
    ): FragmentOrderSharedViewModel = ViewModelProvider(
        fragment.activity!!, ViewModelProviderFactory(FragmentOrderSharedViewModel::class) {
            FragmentOrderSharedViewModel(schedulerProvider, compositeDisposable)
        }).get(FragmentOrderSharedViewModel::class.java)


    @Provides
    fun provideLinearLayoutManager() =
        LinearLayoutManager(fragment.requireContext(), LinearLayoutManager.HORIZONTAL, false)


    @Provides
    fun provideOrderAdapter() = OrderAdapter(fragment.lifecycle, ArrayList())

    @Provides
    fun provideDividerItemDecoration() =
        DividerItemDecoration(fragment.requireContext(), LinearLayout.VERTICAL)

}
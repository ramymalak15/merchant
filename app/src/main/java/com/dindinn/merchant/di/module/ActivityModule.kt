package com.dindinn.merchant.di.module



import androidx.lifecycle.ViewModelProvider
import com.dindinn.merchant.ui.base.BaseActivity
import com.dindinn.merchant.ui.main.MainViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable

@Module
class ActivityModule(private val activity: BaseActivity<*,*>) {

    @Provides
    fun provideMainViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable
    ): MainViewModel = ViewModelProvider(
        activity, ViewModelProviderFactory(MainViewModel::class) {
            MainViewModel(schedulerProvider, compositeDisposable)
        }).get(MainViewModel::class.java)

}
package com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Ingredient
import com.dindinn.merchant.di.component.ViewHolderComponent
import com.dindinn.merchant.ui.base.BaseItemViewHolder
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView


class IngredientItemViewHolder(parent: ViewGroup) :
    BaseItemViewHolder<Ingredient, IngredientItemViewModel>(R.layout.item_ingredient, parent) {

    companion object{
        const val THRESHOLD = 5
    }


    override fun injectDependencies(viewHolderComponent: ViewHolderComponent) =
        viewHolderComponent.inject(this)

    override fun setupView(view: View) {


    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.title.observe(this, {
            it?.run {
                itemView.findViewById<MaterialTextView>(R.id.item_ingredient_title).text = this
            }
        })
        viewModel.available.observe(this, {
            it?.run {
                val quantity = this
                itemView.findViewById<MaterialTextView>(R.id.item_ingredient_available_tv).setBackgroundColor(
                    ContextCompat.getColor(itemView.context,if(quantity <= THRESHOLD) R.color.red else android.R.color.darker_gray)
                )
                itemView.findViewById<MaterialCardView>(R.id.item_ingredient_available_card).strokeColor =
                    ContextCompat.getColor(itemView.context,if(quantity <= THRESHOLD) R.color.red else android.R.color.darker_gray)

                itemView.findViewById<MaterialTextView>(R.id.item_ingredient_available).run {
                    text = quantity.toString()
                    setTextColor(ContextCompat.getColor(itemView.context,if(quantity <= THRESHOLD) R.color.red else R.color.black))
                }
            }
        })
        viewModel.image.observe(this, {
            it?.run {
                itemView.findViewById<AppCompatImageView>(R.id.item_ingredient_iv).setImageResource(this)
            }
        })
    }
}
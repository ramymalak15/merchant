package com.dindinn.merchant.ui.main.fragments.orders.item_item

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.dindinn.merchant.data.remote.response.Addon
import com.dindinn.merchant.data.remote.response.MainItem
import com.dindinn.merchant.ui.base.BaseItemViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class ItemItemViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
): BaseItemViewModel<MainItem>(
    schedulerProvider, compositeDisposable
){
    override fun onCreate() {

    }


    val title:LiveData<String> = Transformations.map(data){
        it.title
    }
    val addons:LiveData<List<Addon>> = Transformations.map(data){
        it.addon
    }
    val quantity:LiveData<Int> = Transformations.map(data){
        it.quantity
    }

}
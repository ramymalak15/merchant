package com.dindinn.merchant.ui.main.fragments.orders.order_item

import android.media.RingtoneManager
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Data
import com.dindinn.merchant.di.component.ViewHolderComponent
import com.dindinn.merchant.ui.base.BaseItemViewHolder
import com.dindinn.merchant.ui.main.fragments.orders.FragmentOrderSharedViewModel
import com.dindinn.merchant.ui.main.fragments.orders.item_item.ItemAdapter
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import javax.inject.Inject
import kotlin.math.ceil


class OrderItemViewHolder(parent: ViewGroup) :
    BaseItemViewHolder<Data, OrderItemViewModel>(R.layout.item_order, parent) {

    companion object {
        const val TAG = "OrderItemViewHolder"
    }

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var itemAdapter: ItemAdapter

    @Inject
    lateinit var dividerItemDecoration: DividerItemDecoration

    @Inject
    lateinit var fragmentOrderSharedViewModel: FragmentOrderSharedViewModel

    private lateinit var acceptedButton: MaterialButton
    private lateinit var expiredButton: MaterialButton
    private lateinit var ordersRv: RecyclerView
    private lateinit var timeProgress: AppCompatRatingBar

    override fun injectDependencies(viewHolderComponent: ViewHolderComponent) =
        viewHolderComponent.inject(this)

    override fun setupView(view: View) {
        ordersRv = itemView.findViewById(R.id.item_order_order_items_rv)
        acceptedButton = itemView.findViewById(R.id.item_order_accept_btn)
        expiredButton = itemView.findViewById(R.id.item_order_expired_btn)
        timeProgress = itemView.findViewById(R.id.item_order_time_progress)

        ordersRv.apply {
            layoutManager = linearLayoutManager
            adapter = itemAdapter
            ContextCompat.getDrawable(itemView.context, R.drawable.divider_layer)?.run {
                dividerItemDecoration.setDrawable(this)
            }
            addItemDecoration(dividerItemDecoration)
        }


        acceptedButton.setOnClickListener {
            viewModel.onAccept()
        }

    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.isAlerted.observe(this, {})
        viewModel.alertedAt.observe(this, {})
        viewModel.id.observe(this, { id ->
            expiredButton.setOnClickListener {
                fragmentOrderSharedViewModel.onExpired(id)
            }
        })
        viewModel.expiredAt.observe(this, {
            it?.run {
                viewModel.startTimer(this)
            }
        })
        viewModel.items.observe(this, {
            it?.run {
                itemAdapter.updateList(this)
            }

        })
        viewModel.isAccepted.observe(this, {
            if (it) {
                acceptedButton.text = itemView.context.getString(R.string.order_accepted)
            }
        })
        viewModel.id.observe(this, {
            it?.run {
                itemView.findViewById<MaterialTextView>(R.id.item_order_order_number).text =
                    itemView.context.getString(R.string.order_number, this)
            }

        })
        viewModel.finishTime.observe(this, {
            it?.run {
                updateCounterView(this)
            }
        })
        viewModel.timeToFinish.observe(this, {
            it?.run {
                updateCounterView(this)
            }


        })
        viewModel.playSound.observe(this, {
            it?.run {
                if (this)
                    playSound()
            }
        })
        viewModel.isExpired.observe(this, {
            it?.run {
                acceptedButton.visibility = if (this) View.GONE else View.VISIBLE
                expiredButton.visibility =
                    if (this) View.VISIBLE else View.GONE
                timeProgress.rating =
                    if (this) 5f else 0f
            }
        })
        viewModel.quantity.observe(this, {
            it?.run {
                itemView.findViewById<MaterialTextView>(R.id.item_order_total_tv).text =
                    itemView.context.resources.getQuantityString(
                        R.plurals.item_quantity,
                        this,
                        this
                    )
            }
        })
        viewModel.createdAt.observe(this, {
            it?.run {
                val spannableString =
                    SpannableString(itemView.context.getString(R.string.at_time, this))
                val textColorSpan = ForegroundColorSpan(
                    ContextCompat.getColor(itemView.context, android.R.color.darker_gray)
                )
                spannableString.setSpan(textColorSpan, 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                itemView.findViewById<MaterialTextView>(R.id.item_order_order_time).text =
                    spannableString
            }

        })

    }

    private fun updateCounterView(time: Long) {
        timeProgress.rating =
            when {
                time <= 0L -> {
                    5f
                }
                time < (1L * 60L * 1000L) -> {
                    4f
                }
                time < (2L * 60L * 1000L) -> {
                    3f
                }
                time < (3L * 60L * 1000L) -> {
                    2f
                }
                time < (4L * 60L * 1000L) -> {
                    1f
                }
                else -> {
                    0f
                }
            }
        if (time > 60000) {
            val minutes = ceil(time.toDouble() / 60000).toInt()
            itemView.findViewById<MaterialTextView>(R.id.item_order_order_expiry).text =
                itemView.context.resources.getQuantityString(
                    R.plurals.item_mins,
                    minutes,
                    minutes
                )
        } else {
            val secs = (time / 1000)
            itemView.findViewById<MaterialTextView>(R.id.item_order_order_expiry).text =
                itemView.context.getString(R.string.item_seconds, if (secs > 0) secs else 0)
        }
    }

    private fun playSound() {
        try {
            val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(
                itemView.context,
                notification
            )
            r.play()
            showMessage(R.string.order_reminder)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
package com.dindinn.merchant.ui.main.fragments.orders

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Data
import com.dindinn.merchant.data.repository.OrdersRepository
import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class OrdersViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    private val ordersRepository: OrdersRepository
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }


    val orders = MutableLiveData<ArrayList<Data>>()
    fun getOrders(){
        if(orders.value == null){
            compositeDisposable.addAll(
                ordersRepository.fetchOrdersList()
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            if(it.status.success){
                                orders.postValue(ArrayList(it.data))
                            }else{
                                messageString.postValue(Resource.error(it.status.message))
                            }

                        },
                        {
                            messageStringId.postValue(Resource.error(R.string.network_connection_error))
                        }
                    )
            )
        }

    }

    fun onRemoveItem(id: Int) {
        orders.value?.find { it.id == id }?.run {
            orders.value?.remove(this)
            orders.setValue(orders.value)
        }
    }
}
package com.dindinn.merchant.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.dindinn.merchant.MerchantApplication
import com.dindinn.merchant.di.component.ActivityComponent
import com.dindinn.merchant.di.component.DaggerActivityComponent
import com.dindinn.merchant.di.module.ActivityModule
import com.dindinn.merchant.utils.common.Status
import com.dindinn.merchant.utils.display.Toaster
import javax.inject.Inject


abstract class BaseActivity<VM : BaseViewModel, VB : ViewBinding> : AppCompatActivity() {

    private var _binding: ViewBinding? = null

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    @Inject
    lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(buildActivityComponent())
        super.onCreate(savedInstanceState)
        _binding = bindingInflater(layoutInflater)
        setContentView(requireNotNull(_binding).root)
        setupView(savedInstanceState)
        setupObservers()
        viewModel.onCreate()
    }

    private fun buildActivityComponent() =
        DaggerActivityComponent
            .builder()
            .applicationComponent((application as MerchantApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()

    protected open fun setupObservers() {
        viewModel.messageString.observe(this, {
            it.data?.run { showMessage(this, it.status) }
        })

        viewModel.messageStringId.observe(this, {
            it.data?.run { showMessage(this,it.status) }
        })
    }

    private fun showMessage(message: String, status: Status) { if(message.isNotEmpty()) Toaster.show(applicationContext, message, status) }

    private fun showMessage(@StringRes resId: Int, status: Status) = Toaster.show(applicationContext, getString(resId),status)

    private fun showMessage(message: String) { if(message.isNotEmpty()) Toaster.show(applicationContext, message) }

    fun showMessage(@StringRes resId: Int) = showMessage(getString(resId))

    private fun showErrorMessage(message: String) { if(message.isNotEmpty()) Toaster.show(applicationContext, message,
        Status.ERROR) }

    fun showErrorMessage(@StringRes resId: Int) = showErrorMessage(getString(resId))

    protected abstract fun bindingInflater(layoutInflater: LayoutInflater): VB

    protected abstract fun injectDependencies(activityComponent: ActivityComponent)

    protected abstract fun setupView(savedInstanceState: Bundle?)

    fun hideKeyBoard(isHidden:Boolean = true) {
        try {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (isHidden)
                imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
            else
                imm.hideSoftInputFromWindow(currentFocus?.windowToken, 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    open fun goBack() = onBackPressed()

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStackImmediate()
        else super.onBackPressed()
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
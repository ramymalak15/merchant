package com.dindinn.merchant.ui.main.fragments.orders.item_item

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.MainItem
import com.dindinn.merchant.di.component.ViewHolderComponent
import com.dindinn.merchant.ui.base.BaseItemViewHolder
import com.google.android.material.textview.MaterialTextView



class ItemItemViewHolder(parent: ViewGroup) :
    BaseItemViewHolder<MainItem, ItemItemViewModel>(R.layout.item_item, parent) {

    override fun injectDependencies(viewHolderComponent: ViewHolderComponent) =
        viewHolderComponent.inject(this)

    override fun setupView(view: View) {

    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.title.observe(this, {
            it?.run {
                itemView.findViewById<MaterialTextView>(R.id.item_item_name).text = this
            }
        })
        viewModel.quantity.observe(this, {
            it?.run {
                itemView.findViewById<MaterialTextView>(R.id.item_item_quantity).text =
                    itemView.context.getString(R.string.item_quantity, this)
            }
        })
        viewModel.addons.observe(this, {
            it?.run {
                itemView.findViewById<LinearLayoutCompat>(R.id.item_addons_lo).removeAllViews()
                this.forEach { addon ->
                    val v = MaterialTextView(itemView.context)
                    v.text = itemView.context.getString(
                        R.string.addons_item_text,
                        addon.quantity,
                        addon.title
                    )
                    itemView.findViewById<LinearLayoutCompat>(R.id.item_addons_lo).addView(v)
                }

            }
        })

    }

}
package com.dindinn.merchant.ui.main.fragments.search.ingredient

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dindinn.merchant.databinding.FragmentIngredientBinding
import com.dindinn.merchant.di.component.FragmentComponent
import com.dindinn.merchant.ui.base.BaseFragment
import com.dindinn.merchant.ui.main.fragments.search.CategoryAdapter.Companion.CATEGORY_ID
import com.dindinn.merchant.ui.main.fragments.search.SearchIngredientSharedViewModel
import com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item.IngredientAdapter
import com.google.android.flexbox.FlexboxLayoutManager
import javax.inject.Inject

class IngredientFragment : BaseFragment<IngredientViewModel, FragmentIngredientBinding>() {

    @Inject
    lateinit var flexboxLayoutManager: FlexboxLayoutManager

    @Inject
    lateinit var ingredientAdapter: IngredientAdapter

    @Inject
    lateinit var searchIngredientSharedViewModel: SearchIngredientSharedViewModel

    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): FragmentIngredientBinding = FragmentIngredientBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) = fragmentComponent.inject(this)

    override fun setupView(view: View) {
        arguments?.getInt(CATEGORY_ID)?.run {
            viewModel.setCategoryId(this)

            binding.fragmentIngredientRv.apply {
                adapter = ingredientAdapter
                layoutManager = flexboxLayoutManager
            }
        }
    }

    override fun setupObservers() {
        super.setupObservers()
        searchIngredientSharedViewModel.searchText.observe(this, {
            it?.run {
                viewModel.onTextChange(this)
            }

        })
        viewModel.categoryId.observe(this, {
            viewModel.getIngredients()
        })
        viewModel.searchText.observe(this, {
            viewModel.getIngredients()
        })
        viewModel.ingredients.observe(this, {
            ingredientAdapter.updateList(it)
        })
    }
}
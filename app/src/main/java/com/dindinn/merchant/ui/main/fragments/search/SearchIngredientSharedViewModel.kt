package com.dindinn.merchant.ui.main.fragments.search

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class SearchIngredientSharedViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val searchText = MutableLiveData<String>()
    fun onTextChange(text: String) {
        searchText.value = text
    }

}
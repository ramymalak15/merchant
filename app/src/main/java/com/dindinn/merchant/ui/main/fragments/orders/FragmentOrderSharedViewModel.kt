package com.dindinn.merchant.ui.main.fragments.orders

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class FragmentOrderSharedViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val expiredId = MutableLiveData<Int>()
    fun onExpired(id: Int) {
        expiredId.postValue(id)
    }

}
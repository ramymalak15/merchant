package com.dindinn.merchant.ui.main.fragments.search

import android.os.Bundle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dindinn.merchant.data.remote.response.Category
import com.dindinn.merchant.ui.base.BaseFragment
import com.dindinn.merchant.ui.main.fragments.search.ingredient.IngredientFragment

class CategoryAdapter(fragment: BaseFragment<*, *>, val categories: ArrayList<Category>) :
    FragmentStateAdapter(fragment) {

    companion object{
        const val CATEGORY_ID = "CATEGORY_ID"
    }

    override fun getItemCount(): Int = categories.size

    override fun createFragment(position: Int): BaseFragment<*, *> {
        val fragment = IngredientFragment()
        val args = Bundle()
        args.putInt(
            CATEGORY_ID,
            categories[position].id
        )
        fragment.arguments = args
        return fragment
    }

    fun updateList(list: List<Category>) {
        categories.clear()
        categories.addAll(list)
        notifyDataSetChanged()
    }


}
package com.dindinn.merchant.ui.main.fragments.search

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Category
import com.dindinn.merchant.data.repository.IngredientRepository
import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class SearchViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    private val ingredientRepository: IngredientRepository
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val categories = MutableLiveData<List<Category>>()
    fun getCategories() {
        compositeDisposable.addAll(
            ingredientRepository.fetchCategories()
                .subscribeOn(
                    schedulerProvider.io()
                )
                .subscribe(
                    {
                        if(it.status.success){
                            categories.postValue(it.categories)
                        }else{
                            messageString.postValue(Resource.error(it.status.message))
                        }
                    },
                    {e->
                        messageStringId.postValue(Resource.error(R.string.unexpected_error))
                        e.printStackTrace()
                    }
                )
        )
    }

    val currentTab = MutableLiveData<Int>()
    fun onTabSelected(selectedTabPosition: Int) {
        currentTab.postValue(selectedTabPosition)
    }

}
package com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.dindinn.merchant.data.remote.response.Ingredient
import com.dindinn.merchant.ui.base.BaseItemViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class IngredientItemViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseItemViewModel<Ingredient>(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val title:LiveData<String> = Transformations.map(data){
        it.title
    }
    val available:LiveData<Int> = Transformations.map(data){
        it.available
    }
    val image:LiveData<Int> = Transformations.map(data){
        it.image
    }

}
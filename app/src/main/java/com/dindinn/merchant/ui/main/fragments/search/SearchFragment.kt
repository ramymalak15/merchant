package com.dindinn.merchant.ui.main.fragments.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.viewpager2.widget.ViewPager2
import com.dindinn.merchant.databinding.FragmentSearchBinding
import com.dindinn.merchant.di.component.FragmentComponent
import com.dindinn.merchant.ui.base.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class SearchFragment : BaseFragment<SearchViewModel, FragmentSearchBinding>() {

    @Inject
    lateinit var categoryAdapter: CategoryAdapter

    @Inject
    lateinit var searchIngredientSharedViewModel: SearchIngredientSharedViewModel

    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): FragmentSearchBinding = FragmentSearchBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) = fragmentComponent.inject(this)

    private val callBack = object: ViewPager2.OnPageChangeCallback(){
        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)
            if(state == ViewPager2.SCROLL_STATE_IDLE){
                viewModel.onTabSelected(binding.fragmentSearchPager.currentItem)
            }

        }
    }

    override fun onDestroyView() {
        binding.fragmentSearchPager.unregisterOnPageChangeCallback(callBack)
        super.onDestroyView()
    }
    override fun setupView(view: View) {
        binding.fragmentSearchPager.adapter = categoryAdapter
        TabLayoutMediator(
            binding.fragmentSearchTabs,
            binding.fragmentSearchPager
        ) { tab, position ->
            tab.text = categoryAdapter.categories[position].name
        }.attach()

        binding.fragmentSearchPager.registerOnPageChangeCallback(callBack)
        binding.fragmentSearchSv.setOnQueryTextListener(
            object:SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    searchIngredientSharedViewModel.onTextChange(query.toString())
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    searchIngredientSharedViewModel.onTextChange(newText?:"")
                    return true
                }

            }
        )
        viewModel.getCategories()
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.currentTab.observe(this, {
            it?.run {
                if(this != binding.fragmentSearchPager.currentItem){
                    binding.fragmentSearchPager.postDelayed({
                        binding.fragmentSearchPager.setCurrentItem(this, false)
                    },200L)
                }
            }
        })
        viewModel.categories.observe(this, {
            it?.run {
                categoryAdapter.updateList(this)
            }
        })
    }
}
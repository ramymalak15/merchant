package com.dindinn.merchant.ui.main.fragments.search.ingredient

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Ingredient
import com.dindinn.merchant.data.repository.IngredientRepository
import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class IngredientViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    private val ingredientRepository: IngredientRepository
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val categoryId = MutableLiveData<Int>()
    val searchText = MutableLiveData<String>()
    val ingredients = MutableLiveData<List<Ingredient>>()
    fun setCategoryId(id: Int) {
        categoryId.value = id
    }

    fun getIngredients() {
        val id = categoryId.value
        val text = searchText.value

        if (id != null) {
            compositeDisposable.addAll(
                ingredientRepository.fetchIngredients(id, text ?: "")
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            if(it.isNullOrEmpty()){
                                messageStringId.postValue(Resource.error(R.string.no_ingredient_match_search))
                            }
                            ingredients.postValue(it)
                        },
                        {e->
                            messageStringId.postValue(Resource.error(R.string.unexpected_error))
                            e.printStackTrace()
                        }
                    )
            )
        }

    }

    fun onTextChange(it: String) {
        searchText.value = it
    }

}
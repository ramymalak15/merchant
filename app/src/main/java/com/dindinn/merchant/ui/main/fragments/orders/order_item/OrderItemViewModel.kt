package com.dindinn.merchant.ui.main.fragments.orders.order_item

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.Data
import com.dindinn.merchant.data.remote.response.MainItem
import com.dindinn.merchant.ui.base.BaseItemViewModel
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OrderItemViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseItemViewModel<Data>(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

    val timeToFinish = MutableLiveData<Long>()
    val isExpired = MutableLiveData<Boolean>()
    val isAccepted: LiveData<Boolean> = Transformations.map(data) {
        it.isAccepted
    }
    val isAlerted: LiveData<Boolean> = Transformations.map(data) {
        it.isAlerted
    }
    val playSound = MutableLiveData<Boolean>()

    fun startTimer(date: Date) {
        compositeDisposable.addAll(
            Observable.interval(1, 1, TimeUnit.SECONDS)
                .takeUntil {
                    Log.d(OrderItemViewHolder.TAG, "isAccepted  = ${isAccepted.value == true}")
                    isAccepted.value == true || date.before(Date())
                }
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        if(isAccepted.value != true){
                            val current = Date()
                            timeToFinish.value = date.time - current.time
                            alertedAt.value?.run {
                                if (time <= current.time && isAlerted.value != true) {
                                    data.value?.isAlerted = true
                                    data.value = data.value
                                    playSound.postValue(true)
                                }
                            }
                            if (date.time - current.time <= 0) {
                                isExpired.value = true
                            }
                        }

                    },
                    {e->
                        e.printStackTrace()
                        messageStringId.postValue(Resource.error(R.string.network_default_error))
                    }
                )
        )
    }

    val id: LiveData<Int> = Transformations.map(data) {
        it.id
    }

    val quantity: LiveData<Int> = Transformations.map(data) { data1 ->
        var sum = 0
        data1.items.forEach {
            sum += it.quantity
        }
        return@map sum
    }
    val createdAt: LiveData<String> = Transformations.map(data) {
        SimpleDateFormat("hh:mma", Locale.getDefault()).format(it.createdAt)
    }

    val items: LiveData<List<MainItem>> = Transformations.map(data) {
        it.items
    }

    val alertedAt: LiveData<Date> = Transformations.map(data) {
        it.alertedAt
    }

    val expiredAt: LiveData<Date> = Transformations.map(data) {
        it.expiredAt
    }
    val finishTime: LiveData<Long> = Transformations.map(data) {
        it.finishTime
    }

    fun onAccept() {
        if (data.value?.isAccepted != true) {
            data.value?.isAccepted = true
            data.value?.finishTime = timeToFinish.value
            data.value = data.value
        }
    }
}
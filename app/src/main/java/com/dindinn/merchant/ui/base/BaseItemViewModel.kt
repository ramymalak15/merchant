package com.dindinn.merchant.ui.base

import androidx.lifecycle.MutableLiveData
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable


abstract class BaseItemViewModel<T : Any>(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(schedulerProvider, compositeDisposable) {

    val data: MutableLiveData<T> = MutableLiveData()

    fun onManualCleared() = onCleared()

    fun updateData(data: T) {
        this.data.postValue(data)
    }
}
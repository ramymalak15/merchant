package com.dindinn.merchant.ui.main

import com.dindinn.merchant.ui.base.BaseViewModel
import com.dindinn.merchant.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class MainViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BaseViewModel(
    schedulerProvider, compositeDisposable
) {
    override fun onCreate() {

    }

}
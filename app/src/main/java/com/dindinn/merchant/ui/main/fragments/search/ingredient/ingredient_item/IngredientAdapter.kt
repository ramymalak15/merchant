package com.dindinn.merchant.ui.main.fragments.search.ingredient.ingredient_item

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.dindinn.merchant.data.remote.response.Ingredient
import com.dindinn.merchant.ui.base.BaseAdapter

class IngredientAdapter(
    parentLifecycle: Lifecycle,
    ingredients: ArrayList<Ingredient>
) : BaseAdapter<Ingredient, IngredientItemViewHolder>(parentLifecycle, ingredients) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = IngredientItemViewHolder(parent)


}
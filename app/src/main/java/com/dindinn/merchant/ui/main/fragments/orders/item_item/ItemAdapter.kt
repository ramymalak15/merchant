package com.dindinn.merchant.ui.main.fragments.orders.item_item

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.dindinn.merchant.data.remote.response.MainItem
import com.dindinn.merchant.ui.base.BaseAdapter

class ItemAdapter(
    parentLifecycle: Lifecycle,
    items: ArrayList<MainItem>
) : BaseAdapter<MainItem, ItemItemViewHolder>(parentLifecycle, items) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ItemItemViewHolder(parent)

}
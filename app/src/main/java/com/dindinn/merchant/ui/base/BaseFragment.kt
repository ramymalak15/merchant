package com.dindinn.merchant.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.dindinn.merchant.MerchantApplication
import com.dindinn.merchant.di.component.DaggerFragmentComponent
import com.dindinn.merchant.di.component.FragmentComponent
import com.dindinn.merchant.di.module.FragmentModule
import com.dindinn.merchant.utils.display.Toaster
import javax.inject.Inject


abstract class BaseFragment<VM : BaseViewModel, VB : ViewBinding> : Fragment() {

    @Inject
    lateinit var viewModel: VM

    private var _binding: ViewBinding? = null

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(buildFragmentComponent())
        super.onCreate(savedInstanceState)
        setupObservers()
        viewModel.onCreate()
    }

    private fun buildFragmentComponent() =
        DaggerFragmentComponent
            .builder()
            .applicationComponent((requireContext().applicationContext as MerchantApplication).applicationComponent)
            .fragmentModule(FragmentModule(this))
            .build()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = bindingInflater(inflater, container)
        return requireNotNull(_binding).root
    }

    protected abstract fun bindingInflater(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) : VB

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    protected open fun setupObservers() {
        viewModel.messageString.observe(this, {
            it.data?.run { showMessage(this) }
        })

        viewModel.messageStringId.observe(this, {
            it.data?.run { showMessage(this) }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
    }


    private fun showMessage(message: String) = context?.let { Toaster.show(it, message) }

    private fun showMessage(@StringRes resId: Int) = showMessage(getString(resId))

    fun goBack() {
        if (activity is BaseActivity<*,*>) (activity as BaseActivity<*,*>).goBack()
    }

    protected abstract fun injectDependencies(fragmentComponent: FragmentComponent)

    protected abstract fun setupView(view: View)
}
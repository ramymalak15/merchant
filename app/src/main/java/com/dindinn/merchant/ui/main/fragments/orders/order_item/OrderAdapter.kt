package com.dindinn.merchant.ui.main.fragments.orders.order_item

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.dindinn.merchant.data.remote.response.Data
import com.dindinn.merchant.ui.base.BaseAdapter

class OrderAdapter(
    parentLifecycle: Lifecycle,
    val orders: ArrayList<Data>
) : BaseAdapter<Data, OrderItemViewHolder>(parentLifecycle, orders) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = OrderItemViewHolder(parent)
    fun removeItemById(id: Int) {
        orders.find { it.id == id }?.run {
            val index = orders.indexOf(this)
            orders.remove(this)
            notifyItemRemoved(index)
        }
    }

}
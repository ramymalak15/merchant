package com.dindinn.merchant.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import com.dindinn.merchant.databinding.ActivityMainBinding
import com.dindinn.merchant.di.component.ActivityComponent
import com.dindinn.merchant.ui.base.BaseActivity

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    override fun bindingInflater(layoutInflater: LayoutInflater): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun injectDependencies(activityComponent: ActivityComponent) = activityComponent.inject(this)

    override fun setupView(savedInstanceState: Bundle?) {

    }

}
package com.dindinn.merchant.ui.main.fragments.orders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.dindinn.merchant.R
import com.dindinn.merchant.databinding.FragmentOrdersBinding
import com.dindinn.merchant.di.component.FragmentComponent
import com.dindinn.merchant.ui.base.BaseFragment
import com.dindinn.merchant.ui.main.fragments.orders.order_item.OrderAdapter
import javax.inject.Inject
import javax.inject.Provider

class OrdersFragment : BaseFragment<OrdersViewModel, FragmentOrdersBinding>() {

    @Inject
    lateinit var linearLayoutManager: Provider<LinearLayoutManager>

    @Inject
    lateinit var orderAdapter: OrderAdapter

    @Inject
    lateinit var fragmentOrderSharedViewModel: FragmentOrderSharedViewModel


    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): FragmentOrdersBinding = FragmentOrdersBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) = fragmentComponent.inject(this)

    override fun setupView(view: View) {
        binding.fragmentOrdersRv.apply {
            layoutManager = linearLayoutManager.get()
            adapter = orderAdapter
        }
        binding.fragmentOrderIngredientBtn.setOnClickListener {
            findNavController().navigate(R.id.action_ordersFragment_to_ingredientFragment)
        }
        viewModel.getOrders()
    }

    override fun setupObservers() {
        super.setupObservers()
        fragmentOrderSharedViewModel.expiredId.observe(this, {
            it?.run {
                orderAdapter.removeItemById(this)
                viewModel.onRemoveItem(this)
            }
        })
        viewModel.orders.observe(this, {
            orderAdapter.updateList(it)
        })
    }
}
package com.dindinn.merchant.utils.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
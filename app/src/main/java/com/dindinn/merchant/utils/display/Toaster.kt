package com.dindinn.merchant.utils.display

import android.content.Context
import android.graphics.PorterDuff
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.dindinn.merchant.R
import com.dindinn.merchant.utils.common.Status

object Toaster {
    fun show(context: Context, text: CharSequence) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
            toast.view?.background?.setColorFilter(
                ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN
            )
            val textView = toast.view?.findViewById(android.R.id.message) as TextView
            textView.setTextColor(ContextCompat.getColor(context, R.color.black))
        }
        toast.show()
    }

    fun show(context: Context, text: CharSequence,status: Status) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
            toast.view?.background?.setColorFilter(
                ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN
            )
            val textView = toast.view?.findViewById(android.R.id.message) as TextView
            when(status){
                Status.SUCCESS->{
                    textView.setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_light))
                }
                Status.ERROR->{
                    textView.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark))
                }
                Status.LOADING->{
                    textView.setTextColor(ContextCompat.getColor(context, android.R.color.black))
                }
                else->{
                    textView.setTextColor(ContextCompat.getColor(context, android.R.color.holo_orange_light))
                }
            }
        }
        toast.show()
    }
}
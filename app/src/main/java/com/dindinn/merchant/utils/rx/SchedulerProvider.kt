package com.dindinn.merchant.utils.rx

import io.reactivex.rxjava3.core.Scheduler
import javax.inject.Singleton

@Singleton
interface SchedulerProvider {

    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler
}
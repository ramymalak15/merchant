package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class OrdersResponse(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "status")
    val status: Status
)
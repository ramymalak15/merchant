package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class Status(
    @Json(name = "message")
    val message: String,
    @Json(name = "statusCode")
    val statusCode: Int,
    @Json(name = "success")
    val success: Boolean
)
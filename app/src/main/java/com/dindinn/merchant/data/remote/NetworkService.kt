package com.dindinn.merchant.data.remote

import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.*
import io.reactivex.rxjava3.core.Single
import java.util.*

open class NetworkService {
    private val startDate = Date()
    open fun doFetchOrdersList(): Single<OrdersResponse> = Single.just(
        OrdersResponse(
            listOf(
                Data(
                    10,
                    Date(startDate.time),
                    Date(startDate.time + (2 * 60000)),
                    Date(startDate.time + (5 * 60000)),
                    listOf(
                        MainItem(
                            20,
                            "Chicken Noodle",
                            2,
                            listOf(
                                Addon(26, "Extra chicken", 2),
                                Addon(27, "Sambal", 1)
                            )
                        ),
                        MainItem(
                            21,
                            "Beef Noodle",
                            3,
                            listOf(
                                Addon(26, "Extra Beef", 3),
                                Addon(27, "Sambal", 5)
                            )
                        ),
                        MainItem(
                            22,
                            "Vegetables Noodle",
                            3,
                            listOf(
                                Addon(26, "Extra Vegetables", 4),
                                Addon(27, "Sambal", 6)
                            )
                        )
                    )


                ),
                Data(
                    11,
                    Date(startDate.time),
                    Date(startDate.time + (0.5 * 60000).toInt()),
                    Date(startDate.time + (1 * 60000)),
                    listOf(
                        MainItem(
                            20,
                            "Special extra large fried rice",
                            1,
                            listOf(
                                Addon(31, "Fried Egg", 3)
                            )
                        )
                    )
                )
            ),
            Status("success", 200, true)
        )
    )

    fun doFetchCategories(): Single<CategoriesResponse> = Single.just(
        CategoriesResponse(
            listOf(
                Category(1, "Main"),
                Category(2, "Sides"),
                Category(3, "Drinks")
            ),
            Status("success", 200, true)
        )
    )

    fun doFetchIngredients(id: Int, searchText: String): Single<List<Ingredient>> = Single.just(
        listOf(
            Ingredient(
                1,
                "Beef Burger",
                5,
                R.drawable.beef_burger,
                1
            ),
            Ingredient(
                2,
                "Chicken Dish",
                20,
                R.drawable.chicken,
                1
            ),
            Ingredient(
                3,
                "Frappuccino chocolate chip",
                10,
                R.drawable.frapachino,
                3
            ),
            Ingredient(
                4,
                "Ice Coffee",
                10,
                R.drawable.ice_coffee,
                3
            ),
            Ingredient(
                5,
                "Meat Shawarma",
                3,
                R.drawable.meat_shawarma,
                1
            ),
            Ingredient(
                6,
                "Pancake",
                10,
                R.drawable.pancake,
                2
            ),
            Ingredient(
                7,
                "Fried Potatoes",
                20,
                R.drawable.potato,
                2
            ),
            Ingredient(
                8,
                "Fried Rice",
                1,
                R.drawable.rice,
                2
            )
        )
    ).map{ list ->
        list.filter { it.categoryId == id && it.title.contains(searchText, true)}
    }

}
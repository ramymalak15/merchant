package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class CategoriesResponse(
    @Json(name = "categories")
    val categories: List<Category>,
    @Json(name = "status")
    val status: Status
)
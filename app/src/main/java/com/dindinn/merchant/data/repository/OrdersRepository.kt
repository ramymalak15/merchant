package com.dindinn.merchant.data.repository


import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.data.remote.response.*
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class OrdersRepository @Inject constructor(private val networkService: NetworkService) {
    fun fetchOrdersList(): Single<OrdersResponse> = networkService.doFetchOrdersList()
}
package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json
import java.util.*

data class Data(
    @Json(name = "id")
    val id: Int,
    @Json(name = "created_at")
    val createdAt: Date,
    @Json(name = "alerted_at")
    val alertedAt: Date,
    @Json(name = "expired_at")
    val expiredAt: Date,
    @Json(name = "items")
    val items: List<MainItem>,
    var isAccepted:Boolean = false,
    var isAlerted:Boolean = false,
    var finishTime:Long? = null
)
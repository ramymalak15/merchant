package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class Ingredient(
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "available")
    val available: Int,
    @Json(name = "image")
    val image: Int,
    @Json(name = "category")
    val categoryId: Int
)
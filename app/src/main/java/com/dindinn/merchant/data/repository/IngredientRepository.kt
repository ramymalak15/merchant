package com.dindinn.merchant.data.repository


import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.data.remote.response.*
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class IngredientRepository @Inject constructor(private val networkService: NetworkService) {

    fun fetchCategories(): Single<CategoriesResponse> = networkService.doFetchCategories()


    fun fetchIngredients(id: Int, searchText: String): Single<List<Ingredient>> = networkService.doFetchIngredients(id, searchText)


}
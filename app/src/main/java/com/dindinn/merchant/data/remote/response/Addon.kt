package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class Addon(
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "quantity")
    val quantity: Int
)
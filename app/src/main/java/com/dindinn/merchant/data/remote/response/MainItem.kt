package com.dindinn.merchant.data.remote.response


import com.squareup.moshi.Json

data class MainItem(
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "quantity")
    val quantity: Int,
    @Json(name = "addon")
    val addon: List<Addon>
)
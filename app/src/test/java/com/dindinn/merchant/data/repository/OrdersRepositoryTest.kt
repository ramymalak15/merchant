package com.dindinn.merchant.data.repository

import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.data.remote.response.OrdersResponse
import com.dindinn.merchant.data.remote.response.Status
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class OrdersRepositoryTest {

    @Mock
    private lateinit var networkService: NetworkService


    private lateinit var ordersRepository: OrdersRepository

    @Before
    fun setUp() {
        ordersRepository = OrdersRepository(networkService)
    }

    @Test
    fun fetchOrdersList_requestDoFetchOrdersList() {
        doReturn(
            Single.just(
                OrdersResponse(
                    listOf(),
                    Status("message", 200, true)
                )
            )
        )
            .`when`(networkService)
            .doFetchOrdersList()

        ordersRepository.fetchOrdersList()

        verify(networkService).doFetchOrdersList()

    }



}
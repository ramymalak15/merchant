package com.dindinn.merchant.data.repository

import com.dindinn.merchant.data.remote.NetworkService
import com.dindinn.merchant.data.remote.response.CategoriesResponse
import com.dindinn.merchant.data.remote.response.Ingredient
import com.dindinn.merchant.data.remote.response.Status
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class IngredientRepositoryTest {

    @Mock
    private lateinit var networkService: NetworkService


    private lateinit var ingredientRepository: IngredientRepository

    @Before
    fun setUp() {
        ingredientRepository = IngredientRepository(networkService)
    }

    @Test
    fun fetchCategories_requestDoFetchCategories() {
        doReturn(
            Single.just(
                CategoriesResponse(
                    listOf(),
                    Status("message", 200, true)
                )
            )
        )
            .`when`(networkService)
            .doFetchCategories()

        ingredientRepository.fetchCategories()

        verify(networkService).doFetchCategories()

    }


    @Test
    fun fetchIngredients_requestDoFetchIngredients() {
        val id = 1
        val text = "text"
        doReturn(
            Single.just(
                listOf<Ingredient>()
            )
        )
            .`when`(networkService)
            .doFetchIngredients(id, text)

        ingredientRepository.fetchIngredients(id, text)

        verify(networkService).doFetchIngredients(id, text)

    }

}
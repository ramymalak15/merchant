package com.dindinn.merchant.ui.main.fragments.search.ingredient

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dindinn.merchant.R
import com.dindinn.merchant.data.remote.response.*
import com.dindinn.merchant.data.repository.IngredientRepository
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.TestSchedulerProvider
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class IngredientViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var ingredientRepository: IngredientRepository

    @Mock
    private lateinit var ingredientObserver: Observer<List<Ingredient>>

    @Mock
    private lateinit var messageStringObserverId: Observer<Resource<Int>>

    private lateinit var testScheduler: TestScheduler

    private lateinit var ingredientViewModel: IngredientViewModel

    @Before
    fun setUp() {
        val compositeDisposable = CompositeDisposable()
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        ingredientViewModel = IngredientViewModel(
            testSchedulerProvider,
            compositeDisposable,
            ingredientRepository
        )
        ingredientViewModel.ingredients.observeForever(ingredientObserver)
        ingredientViewModel.messageStringId.observeForever(messageStringObserverId)
    }

    @Test
    fun givenIngredientMatch_whenGettingIngredients_shouldPostIngredients() {
        val ingredients = listOf(
            Ingredient(
                1,
                "Beef Burger",
                5,
                R.drawable.beef_burger,
                1
            )
        )
        val id = 1
        val searchText = "search"
        ingredientViewModel.categoryId.value = id
        ingredientViewModel.searchText.value = searchText
        doReturn(Single.just(ingredients))
            .`when`(ingredientRepository)
            .fetchIngredients(id, searchText)
        ingredientViewModel.getIngredients()
        testScheduler.triggerActions()
        assert(ingredientViewModel.ingredients.value?.size == 1)
        verify(ingredientObserver).onChanged(ingredients)
    }


    @Test
    fun givenNoIngredientMatch_whenGettingIngredients_shouldPostErrorMessage() {
        val ingredients = listOf<Ingredient>()
        val id = 1
        val searchText = "search"
        ingredientViewModel.categoryId.value = id
        ingredientViewModel.searchText.value = searchText
        doReturn(Single.just(ingredients))
            .`when`(ingredientRepository)
            .fetchIngredients(id, searchText)
        ingredientViewModel.getIngredients()
        testScheduler.triggerActions()
        assert(ingredientViewModel.ingredients.value?.size == 0)
        verify(ingredientObserver).onChanged(emptyList())
        assert(ingredientViewModel.messageStringId.value == Resource.error(R.string.no_ingredient_match_search))
        verify(messageStringObserverId).onChanged(Resource.error(R.string.no_ingredient_match_search))
    }

    @After
    fun tearDown() {
        ingredientViewModel.ingredients.removeObserver(ingredientObserver)
        ingredientViewModel.messageStringId.removeObserver(messageStringObserverId)
    }

}
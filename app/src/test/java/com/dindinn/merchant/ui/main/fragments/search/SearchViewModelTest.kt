package com.dindinn.merchant.ui.main.fragments.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dindinn.merchant.data.remote.response.*
import com.dindinn.merchant.data.repository.IngredientRepository
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.TestSchedulerProvider
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var ingredientRepository: IngredientRepository

    @Mock
    private lateinit var searchObserver: Observer<List<Category>>

    @Mock
    private lateinit var messageStringObserver: Observer<Resource<String>>

    private lateinit var testScheduler: TestScheduler

    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun setUp() {
        val compositeDisposable = CompositeDisposable()
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        searchViewModel = SearchViewModel(
            testSchedulerProvider,
            compositeDisposable,
            ingredientRepository
        )
        searchViewModel.categories.observeForever(searchObserver)
        searchViewModel.messageString.observeForever(messageStringObserver)
    }

    @Test
    fun givenServerResponseSuccess_whenGettingCategories_shouldPostCategories() {
        val categoriesResponse =  CategoriesResponse(
            listOf(
                Category(1, "Main"),
                Category(2, "Sides"),
                Category(3, "Drinks")
            ),
            Status("success", 200, true)
        )
        doReturn(Single.just(categoriesResponse))
            .`when`(ingredientRepository)
            .fetchCategories()
        searchViewModel.getCategories()
        testScheduler.triggerActions()
        assert(searchViewModel.categories.value?.size == 3)
        verify(searchObserver).onChanged(categoriesResponse.categories)
    }

    @Test
    fun givenServerResponseFailed_whenGettingCategories_shouldPostErrorMessage() {
        val categoriesResponse =  CategoriesResponse(
            listOf(
                Category(1, "Main"),
                Category(2, "Sides"),
                Category(3, "Drinks")
            ),
            Status("message", 200, false)
        )
        doReturn(Single.just(categoriesResponse))
            .`when`(ingredientRepository)
            .fetchCategories()
        searchViewModel.getCategories()
        testScheduler.triggerActions()
        assert(searchViewModel.messageString.value == Resource.error("message"))
        verify(messageStringObserver).onChanged(Resource.error("message"))
    }

    @After
    fun tearDown() {
        searchViewModel.categories.removeObserver(searchObserver)
        searchViewModel.messageString.removeObserver(messageStringObserver)
    }

}
package com.dindinn.merchant.ui.main.fragments.orders

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dindinn.merchant.data.remote.response.Data
import com.dindinn.merchant.data.remote.response.OrdersResponse
import com.dindinn.merchant.data.remote.response.Status
import com.dindinn.merchant.data.repository.OrdersRepository
import com.dindinn.merchant.utils.common.Resource
import com.dindinn.merchant.utils.rx.TestSchedulerProvider
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import kotlin.collections.ArrayList

@RunWith(MockitoJUnitRunner::class)
class OrdersViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var ordersRepository: OrdersRepository

    @Mock
    private lateinit var ordersObserver: Observer<ArrayList<Data>>

    @Mock
    private lateinit var messageStringObserver: Observer<Resource<String>>

    private lateinit var testScheduler: TestScheduler

    private lateinit var ordersViewModel: OrdersViewModel

    @Before
    fun setUp() {
        val compositeDisposable = CompositeDisposable()
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        ordersViewModel = OrdersViewModel(
            testSchedulerProvider,
            compositeDisposable,
            ordersRepository
        )
        ordersViewModel.orders.observeForever(ordersObserver)
        ordersViewModel.messageString.observeForever(messageStringObserver)
    }

    @Test
    fun givenServerResponseSuccess_whenGettingOrders_shouldPostOrders() {
        val ordersResponse = OrdersResponse(
            listOf(
                Data(1, Date(), Date(), Date(), listOf())
            ),
            Status("message", 200, true)
        )
        doReturn(Single.just(ordersResponse))
            .`when`(ordersRepository)
            .fetchOrdersList()
        ordersViewModel.getOrders()
        testScheduler.triggerActions()
        assert(ordersViewModel.orders.value?.size == 1)
        verify(ordersObserver).onChanged(ArrayList(ordersResponse.data))
    }

    @Test
    fun givenServerResponseFailed_whenGettingOrders_shouldPostErrorMessage() {
        val ordersResponse = OrdersResponse(
            listOf(
                Data(1, Date(), Date(), Date(), listOf())
            ),
            Status("message", 200, false)
        )
        doReturn(Single.just(ordersResponse))
            .`when`(ordersRepository)
            .fetchOrdersList()
        ordersViewModel.getOrders()
        testScheduler.triggerActions()
        assert(ordersViewModel.messageString.value == Resource.error("message"))
        verify(messageStringObserver).onChanged(Resource.error("message"))
    }

    @After
    fun tearDown() {
        ordersViewModel.orders.removeObserver(ordersObserver)
        ordersViewModel.messageString.removeObserver(messageStringObserver)
    }

}